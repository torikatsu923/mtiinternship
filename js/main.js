document.getElementById("hello_text").textContent = "はじめてのJavaScript"

var count = 0;
var cells;

//ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern:[
            [1,1,1,1]
        ]
    },
    o: {
        class: "o",
        pattern:[
            [1,1],
            [1,1]
        ]
    },
    t: {
        class: "t",
        pattern:[
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern:[
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern:[
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern:[
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern:[
            [0, 0, 1],
            [1, 1, 1]
        ]
    },

}

loadTable();
setInterval(function() {
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
    //ゲームオーバーか判定
    for (var row = 0; row < 2; row ++) {
        for (var col = 0; col < 9; col ++) {
            if(cells[row][col].className !== "" && cells[row][col].blockNum !== fallingBlockNum) {
                alert("GAME OVER");
            }
        }
    }
    if(hasFallingBlock()) {
        fallBlocks();//落下中のブロックがあればブロックを落下させる
    } else { //なければ
        deleteRow();//そろっている行を消す
        generateBlock(); //ランダムにブロックを作成する
    }
}, 500);

/*----関数処理------------------------------------*/

/* テーブル読み込み */
function loadTable() {
    var td_array = document.getElementsByTagName("td");
    cells = [];
    var index = 0;
    for (var row = 0; row < 20; row ++) {
        cells[row] = [];
        for (var col = 0; col < 10; col++) {
            cells[row][col] = td_array[index];
            index ++;
        }
    }
}

/* ブロック落下処理 */
function fallBlocks() {
    //一番下の行にブロックがあれば落下中のフラグをfalseにする
    for(var col = 0; col < 10; col ++) {
        if(cells[19][col].blockNum === fallingBlockNum) {
            isFalling = false;
            return;
        }
    }
    //1マス下に別のブロックがないか
    for(var row = 18; row >= 0; row --) {
        for (var col = 0; col < 10; col ++) {
            if(cells[row][col].blockNum === fallingBlockNum) {
                if(cells[row + 1][col].className !== ""&& cells[row + 1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    return
                }
            }
        }
    }
    //下から2番目の行から繰り返しクラスを下げていく
    for (var row = 18; row >= 0; row --) {
        for(var col = 0; col < 10; col ++) {
            if(cells[row][col].blockNum === fallingBlockNum) {
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

//落下中のブロックがあるか確認する
var isFalling = false;
function hasFallingBlock() {
    return isFalling
}

//そろっている行を消す
function deleteRow() {
    for (var row = 19; row >= 0; row --) {
        var canDelete = true;
        for (var col = 0; col < 10; col ++) {
            if(cells[row][col].className === "") {
                canDelete = false;
            }
        }
        if(canDelete) {
            for (var col = 0; col < 10; col ++) {
                cells[row][col].className = "";
            }
            //上の行を一個ずつ落とす
            for (var downRow = row - 1; downRow >= 0; downRow --) {
                for (var col = 0; col < 10; col ++) {
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}

//ランダムにブロックを生成する
var fallingBlockNum = 0;
function generateBlock() {
    //ブロックパターンを選ぶ
    var keys = Object.keys(blocks)
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    //ブロックの配置
    var pattern = nextBlock.pattern
    for(var row = 0; row < pattern.length; row ++) {
        for (var col = 0; col < pattern[row].length; col ++) {
            if(pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum
            }
        }
    }
    //落下中のブロックがあるフラグを立てる
    fallingBlockNum = nextFallingBlockNum;
    isFalling = true;
}

// キーボードイベントを監視する
document.addEventListener("keydown", onKeyDown);

// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
  if (event.keyCode === 37) {
    moveLeft();
  } else if (event.keyCode === 39) {
    moveRight();
  }
}

function moveRight() {
// ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLeft() {
// ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}